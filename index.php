<?php
/*--------------------------------------------*/
/*               Basic Setup                  */
/*--------------------------------------------*/

session_start();
error_reporting(-1);
ini_set('display_errors', 1);

require('config/config.php');
require('config/classLoader.php');
require('config/BDD.php');


/*--------------------------------------------*/
/*                 Routing                    */
/*       controller/method/parameter          */
/*--------------------------------------------*/

$uri = array_values(array_filter(explode('/', $_SERVER['REQUEST_URI'])));
$go_controller = (isset($uri[1])) ? $uri[1] : false;
$go_method = (isset($uri[2])) ? $uri[2] : false;
$go_param = (isset($uri[3])) ? $uri[3] : false;

if($go_controller !== false AND is_file(controlers.$go_controller.'.php'))
{
  $constr = ucfirst(strtolower($go_controller));
  $OBcontroler = new $constr();
  if($go_method !== false AND method_exists($OBcontroler,$go_method))
  {
    $OBcontroler->$go_method($go_param);
  }
  else
  {
    $OBcontroler->index($go_param);
  }
}
else
{
  $OBcontroler = new $defaultController();
  $OBcontroler->$defautlMethod($go_param);
}
