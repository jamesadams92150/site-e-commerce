-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 02 jan. 2020 à 22:59
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `g4_ecommerce`
--
CREATE DATABASE IF NOT EXISTS `g4_ecommerce` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `g4_ecommerce`;

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `libelle` varchar(200) NOT NULL,
  `id_cat_parent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `date_creation` date NOT NULL,
  `date_livraison` date NOT NULL,
  `statut` varchar(50) NOT NULL,
  `prix_total` float NOT NULL,
  `adr_liv_numero` int(11) NOT NULL,
  `adr_liv_adresse` int(50) NOT NULL,
  `adr_liv_cp` varchar(10) NOT NULL,
  `adr_liv_ville` varchar(30) NOT NULL,
  `adr_liv_pays` varchar(30) NOT NULL,
  `adr_fac_numero` int(11) NOT NULL,
  `adr_fac_adresse` varchar(50) NOT NULL,
  `adr_fac_cp` varchar(10) NOT NULL,
  `adr_fac_ville` varchar(30) NOT NULL,
  `adr_fac_pays` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
CREATE TABLE IF NOT EXISTS `commentaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produit` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `date` date NOT NULL,
  `message` text NOT NULL,
  `note` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `email` varchar(60) NOT NULL,
  `sujet` varchar(30) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `adr_numero` smallint(6) NOT NULL,
  `adr_adresse` varchar(200) NOT NULL,
  `adr_cp` varchar(10) NOT NULL,
  `adr_ville` varchar(30) NOT NULL,
  `adr_pays` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `etp_deno_sociale` varchar(60) NOT NULL,
  `etp_domaine_activite` varchar(60) NOT NULL,
  `date_inscription` date NOT NULL,
  `date_desactivation` date NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `devis`
--

DROP TABLE IF EXISTS `devis`;
CREATE TABLE IF NOT EXISTS `devis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_service` int(11) NOT NULL,
  `date_creation` date NOT NULL,
  `date_conclusion` date NOT NULL,
  `id_commercial` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `prix` float NOT NULL,
  `statut` varchar(30) NOT NULL,
  `quantite` int(11) NOT NULL,
  `description` text NOT NULL,
  `enl_diff_adr` tinyint(1) NOT NULL,
  `enl_adr_numero` int(11) NOT NULL,
  `enl_adr_adresse` varchar(30) NOT NULL,
  `enl_adr_cp` varchar(10) NOT NULL,
  `enl_adr_ville` varchar(30) NOT NULL,
  `enl_adr_pays` varchar(30) NOT NULL,
  `message` text NOT NULL,
  `surface` int(11) NOT NULL,
  `rec_nb_employes` int(11) NOT NULL,
  `pdc_type` varchar(30) NOT NULL,
  `rpu_type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `jonction_promo`
--

DROP TABLE IF EXISTS `jonction_promo`;
CREATE TABLE IF NOT EXISTS `jonction_promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produit` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `id_promotion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ligne_commande`
--

DROP TABLE IF EXISTS `ligne_commande`;
CREATE TABLE IF NOT EXISTS `ligne_commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_commande` int(11) NOT NULL,
  `id_produit` int(11) NOT NULL,
  `quantite` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `email` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(200) NOT NULL,
  `prix` float NOT NULL,
  `description` text NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `resume` text NOT NULL,
  `delai_livraison` int(11) NOT NULL,
  `notation` float NOT NULL,
  `nb_consultations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `promotion`
--

DROP TABLE IF EXISTS `promotion`;
CREATE TABLE IF NOT EXISTS `promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `libelle` varchar(200) NOT NULL,
  `is_prod` tinyint(1) NOT NULL,
  `new_prix` float NOT NULL,
  `is_cat` tinyint(1) NOT NULL,
  `pourcentage` float NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `libelle`) VALUES
(1, 'Client'),
(2, 'Professionnel'),
(3, 'Commercial'),
(4, 'Administrateur'),
(5, 'Développeur');

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `libelle` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `resume` text NOT NULL,
  `prix` float NOT NULL,
  `photo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `statut_commande`
--

DROP TABLE IF EXISTS `statut_commande`;
CREATE TABLE IF NOT EXISTS `statut_commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `statut_commande`
--

INSERT INTO `statut_commande` (`id`, `libelle`) VALUES
(1, 'Panier'),
(2, 'Paiement accepté'),
(3, 'En cours de préparation'),
(4, 'Expédiée'),
(5, 'Livré');

-- --------------------------------------------------------

--
-- Structure de la table `statut_devis`
--

DROP TABLE IF EXISTS `statut_devis`;
CREATE TABLE IF NOT EXISTS `statut_devis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `statut_devis`
--

INSERT INTO `statut_devis` (`id`, `libelle`) VALUES
(1, 'En attente'),
(2, 'Pris en charge'),
(3, 'En attente de votre réponse'),
(4, 'En attente d\'une réponse'),
(5, 'Réalisé / Clôturé');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
