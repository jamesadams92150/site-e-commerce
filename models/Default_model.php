<?php

class Default_model extends Model
{

  private $_table = "MY_TABLE";

  private static $_singleton = null;

  public static function load()
  {
    if (Default_model::$_singleton == null) {
      Default_model::$_singleton = new Default_model();
    }
    return Default_model::$_singleton;
  }

  public function __construct()
  {
    parent::__construct();
  }

  public function get($id = FALSE)
  {
    $id = ($id !== FALSE && ((int) $id) > 0) ? (int) $id : false;

    if ($id === FALSE) {
      $data = $this->db->lire("Select * from " . $this->_table);
    } else {
      if ($id === false)
      return false;
      $cur = $this->db->prep("Select * from " . $this->_table . " where id = :id");
      $data = $this->db->lirePrep($cur, array(
        ":id" => $id
      ));
    }
    if ($data === false)
    return false;
    return $data;
  }

  public function get_ameliore($WHERE = FALSE, $ORDERBY = FALSE, $LIMIT1 = FALSE, $LIMIT2 = FALSE)
  {
    $req = "Select * from " . $this->vue;
    if (! ($WHERE === FALSE)) {
      $req .= " WHERE";
      $i = sizeof($WHERE);
      foreach ($WHERE as $key => $value) {
        $value = (is_numeric($value)) ? (int) $value : "\"" . $value . "\"";
        $req .= " $key = $value";
        $req .= ($i != 1) ? "AND" : "";
        $i --;
      }
    }
    $req = $this->orderby($req, $ORDERBY);
    $LIMIT1 = (int) $LIMIT1;
    $LIMIT2 = (int) $LIMIT2;
    if ($LIMIT1 !== FALSE && $LIMIT2 !== FALSE && $LIMIT1 >= 0 && $LIMIT2 > 0) {
      $req .= " LIMIT $LIMIT1,$LIMIT2";
    }
    return $this->db->lire($req);
  }

  private function orderby($req, $ORDERBY = FALSE)
  {
    if ($ORDERBY !== FALSE) {
      $req .= " ORDER BY";
      foreach ($ORDERBY as $value) {
        if (! (strtolower($value) == "desc" || strtolower($value) == "asc" || $value == ""))
        return $req;
      }
      $i = sizeof($ORDERBY);
      foreach ($ORDERBY as $key => $value) {
        $req .= " $key $value";
        $req .= ($i != 1) ? "," : "";
        $i --;
      }
    }
    return $req;
  }

  public function update($tab)
  {

    $req = 'UPDATE ' . $this->_table . ' SET `CHAMP`= :champ WHERE `id`= :id';
    $cur = $this->db->prep($req);

    $variable = array(
      ":champ" => $tab['champ'],
      ":id" => $tab['id']
    );

    $rep = $this->db->ecrirePrep($cur, $variable);

    return $rep;
  }

  public function delete($id)
  {
    $req = "SELECT COUNT(*) as nb from " . $this->vue . " WHERE id = " . $id;
    $resultat = $this->db->lire($req);

    if ($resultat[0]['nb'] == 1) {
      $req_del = "DELETE FROM " . $this->_table . " WHERE id = " . $id;
      $res_del = $this->db->exec($req_del);
      log_message('Suppression du Default id ' . $id);
    } else {
      log_erreur('Default ' . $id . ' n\'est pas présent dans la base de données');
    }
  }

  public function idMax()
  {
    $res = $this->db->lire("SELECT max(id) as max FROM " . $this->_table);
    return ((int) $res[0]['max']);
  }
}
?>
