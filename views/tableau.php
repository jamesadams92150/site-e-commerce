<?php

/*Si pas données dans un tableau, on prévient, sinon on affiche */

if(empty($data["data"]) || !isset($data["data"]) || empty($data["data"][0]) || !isset($data["data"][0]))
{
  echo '<tr> Aucune donnée disponible, essayez avec des critères de tri différents</tr>';
}
else
{
  foreach ($data["data"] as $value) {
    // code affichage de chaque coureur
    echo "<tr>";
    foreach ($value as $key => $sub_value) {
      if(!in_array($key,$NonAffichable))echo "<td>".$sub_value."</td>";
    }
    if(sizeof($Affichable) != 0)
    {
      foreach ($Affichable as $key => $value2) {
        $tabmethode = explode(":",$value2);

        $controler = new $tabmethode[0]();
        $str = $tabmethode[1];
        $controler->$str($value);
      }
    }
    echo "</tr>";
  }
}
?>
