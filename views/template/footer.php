
</div> <!-- Div container générale -->
</main>

<footer class="page-footer blue-grey darken-3">
  <div class="container">
    <div class="row">
      <div class="col l6 s12 white-text">
        <h5>Quelques liens</h5>
        <p>Lien CGV</p>
        <p>Lien Mentions Légales</p>
      </div>
      <div class="col l6 s12 white-text">
        <h5>Autre chose ?</h5>
          <p>Contact</p>
          <p>
            <!-- Google translate element -->
            <div id="google_translate_element" title="Outil de traduction instanée"></div><script type="text/javascript">
            function googleTranslateElementInit() {
              new google.translate.TranslateElement({pageLanguage: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
            }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
            <!-- End of Google translate element -->
          </p>
      </div>
    </div>
  </div>

  <div class="footer-copyright">
    <div class="container">
        © Framework par Bastien Champin
    </div>
  </div>

</footer>


<!-- Scripts -->
<script src="core/js/materialize.js"></script>
<script src="core/js/init.js"></script>

</body>
</html>
