<!DOCTYPE html>
<html lang="fr">
<head>

  <!-- Meta tags  -->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <meta charset="UTF-8">

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="<?=css?>framework.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?=css?>custom.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <!-- Icônes  -->
  <link rel="icon" type="image/x-icon" href="<?=img?>favicon.png" />
  <link rel="shortcut icon" type="image/x-icon" href="<?=img?>favicon.png" />

  <!-- Title -->
  <title>Mon Titre</title>
</head>
<body>
  <?php include(views."/template/nav.php"); ?>
